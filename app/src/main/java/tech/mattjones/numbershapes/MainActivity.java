package tech.mattjones.numbershapes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    class Number {
        int number;

        public boolean isSquare() {
            double squareRoot = Math.sqrt(number);
            if (squareRoot == Math.floor(squareRoot)) {
                return true;
            } else {
                return false;
            }

        }

        public boolean isTriangular() {
            int x = 1;
            int tri = 1;
            while (tri < number) {
                x++;
                tri = tri + x;
            }
            if (tri == number) {
                return true;
            } else {
                return false;
            }
        }
    }

    public void testNumber(View view) {
        EditText usersNumber = (EditText) findViewById(R.id.usersNumber);
        String message = "";
        if (usersNumber.getText().toString().isEmpty()) {
            message = "You need to enter a number.";
        } else {
            Number myNumber = new Number();
            myNumber.number = Integer.parseInt(usersNumber.getText().toString());

            if (myNumber.isSquare()) {
                if (myNumber.isTriangular()) {
                    message = "Well done! " + myNumber.number + " is both a square and a triangle! How odd!";
                } else {
                    message = myNumber.number + " is for real a square, but def not a triangle.";
                }
            } else {
                if (myNumber.isTriangular()) {
                    message = myNumber.number + " is triangular, but not square.";
                } else {
                    message = myNumber.number + " is just a normal number, neither square nor triangular.";
                }

            }

            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }

}





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
